using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	protected int score, lives;
	protected float bounceDirection, ballDistanceX, ballDistanceY;
	public int ballSpeed = 20;
	protected bool readyToLaunch;
	public TextMesh livesMesh;
	public TextMesh highScore;
	private GameObject paddle;
	

	// Use this for initialization
	void Start () {
		gameObject.renderer.material.color = Color.white;
		readyToLaunch = true;
		score = 0;
		lives = 3;
		paddle = GameObject.Find("Paddle");
}
	
	// Update is called once per frame
	void Update () {
		//Checks to see if the ball is ready to launch
		if(readyToLaunch){
			transform.position = new Vector3(paddle.rigidbody.position.x, paddle.rigidbody.position.y + 2, 0);
		    if (Input.GetKeyDown (KeyCode.Space)){
				readyToLaunch = false;
				rigidbody.velocity = Vector3.one;
				rigidbody.velocity = new Vector3(rigidbody.velocity.x * 2, 
												 rigidbody.velocity.y * ballSpeed, 
												 rigidbody.velocity.z);
			}
		} else {
			rigidbody.velocity = 20 * (rigidbody.velocity.normalized);
			Vector3 v = rigidbody.velocity;
			Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
			Vector3 bottomLeft = Camera.main.WorldToScreenPoint(transform.position - collider.bounds.extents);
				
			//Removes the Lives and HighScore if the ball comes near them so the player can see
			//I would have liked the fade the lives so the player can still see them but I could
			//not figure out how so I removed them completly
			ballDistanceX = livesMesh.transform.position.x - rigidbody.position.x;
			ballDistanceY = rigidbody.position.y - livesMesh.transform.position.y;
			if(ballDistanceX < 6 && ballDistanceY < 10){
				livesMesh.renderer.enabled = false;
				highScore.renderer.enabled = false;
			} else {
				livesMesh.renderer.enabled = true;
				highScore.renderer.enabled = true;
			}
				
			//Sets the bounds for the ball and restarts the ball if it goes out
			if (topRight.y > 1 ){
				rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
		    }
			if (topRight.x > 1){
				rigidbody.velocity = new Vector3(-Mathf.Abs(v.x), v.y, v.z);
			}
			if (bottomLeft.x < 1){
				rigidbody.velocity = new Vector3(Mathf.Abs(v.x), v.y, v.z);
			}
			if (bottomLeft.y < 1){
				rigidbody.velocity = Vector3.zero;
				lives--;
				if (lives == 0){
					Application.LoadLevel("Breakout");
				}
				livesMesh.text = "Lives: " + lives.ToString();
				readyToLaunch = true;
				}
		}
	}
	
	void OnCollisionEnter(Collision col){
		//Allows the player to control where the ball will go when it hits the paddle.
		if(col.gameObject == paddle){
			Vector3 v = rigidbody.velocity;
			bounceDirection = paddle.rigidbody.position.x - rigidbody.position.x;
			rigidbody.velocity = new Vector3(-bounceDirection, v.y, v.z);
		}
	}
}