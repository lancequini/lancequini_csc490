using UnityEngine;
using System.Collections;

public class BrickScript : MonoBehaviour {
	protected int breakPoints = 10;
	protected int totalScore = 0;
	public TextMesh scoreMesh;
	
	void Start(){
		gameObject.renderer.material.color = Color.red;
	}
	
	void OnCollisionEnter(Collision col){
		Destroy(gameObject);
		totalScore = int.Parse(scoreMesh.text.ToString()) + breakPoints;
		if (totalScore == 360){
			Application.LoadLevel("Breakout");
		}
		scoreMesh.text = totalScore.ToString();
	}
}
