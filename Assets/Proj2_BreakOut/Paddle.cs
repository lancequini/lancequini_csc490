using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
	public float speed = 10;
	// Use this for initialization
	void Start () {
		gameObject.renderer.material.color = Color.blue;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 v = rigidbody.velocity;
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToScreenPoint(transform.position - collider.bounds.extents);
		
		rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed;
		if (topRight.x > 1){
			rigidbody.velocity = new Vector3(v.x - 1, v.y, v.z);
		}
		if (bottomLeft.x < 1){
			rigidbody.velocity = new Vector3(v.x + 1, v.y, v.z);
		}
	}
}
